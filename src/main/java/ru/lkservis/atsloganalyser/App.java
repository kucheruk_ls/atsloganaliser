package ru.lkservis.atsloganalyser;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Стартовый класс утилиты
 *файл конфигурации для программы AtsLogAnalayser
 *кодировка файла UTF8 no BOM
 *@author leonid.s. kucheruk shtormlbt@mail.ru
 *программа предназначена для отправки смс сообщений пользователей из файлов находящихся в WorkDir
 *
 *файл должен находиться в директории программы либо задан в качестве параметра при запуске иначе программа не получит
 * параметров и не запуститься.
 *в случае задания конфигурационного файла в качестве параметра указываем полный путь к файлу.
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        String s = null;
        if(args.length>0){
            s = args[0];
        }else{
            //System.out.println(System.getProperty("user.dir"));
            s = System.getProperty("user.dir")+"\\config.properties";
        }
        File file = new File(s);
        System.out.println(file.getAbsoluteFile());
        ConfigReader configReader = null;
        try{
            configReader = new ConfigReader(file);
        }catch (IOException e){
            e.printStackTrace();
        }

        //System.out.println(configReader.getControlDir()); // тестовая печать рабочей дирректории

        LogAnalyser logAnalyser = new LogAnalyser();
        HashMap<String, ArrayList<String>> callMap = logAnalyser.run();

        ReportCreator reportCreator = new ReportCreator();
        reportCreator.reportGenerator(callMap);

        ZipArhiver zipArhiver = new ZipArhiver();
        try{
            Path zipArhName = zipArhiver.createZip(Paths.get(ConfigReader.getReportDir()));
            System.out.println("Создан архив: "+zipArhName.toString());
            ZipSendMail zipSendMail = new ZipSendMail();
            zipSendMail.send(zipArhName.toString(),zipArhName.getFileName().toString());

            Files.delete(zipArhName);

            //и очтеты тоже удалить
            Path path = Paths.get(ConfigReader.getReportDir());
            FileManager fileManager = new FileManager(path);
            List<Path> fileDell = fileManager.getFileList();

            for(Path p:fileDell){
                Path pd = path.resolve(p);
                Files.delete(pd);
            }

        }catch (Exception e){
            e.printStackTrace();
        }



    }
}
