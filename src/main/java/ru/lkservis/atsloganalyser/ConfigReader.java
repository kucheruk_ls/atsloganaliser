package ru.lkservis.atsloganalyser;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Properties;

public class ConfigReader {
    static File file;
    static Properties properties;

    public ConfigReader(File file) throws IOException{
        properties = new Properties();
        properties.load(new FileReader(file));
        }

    /**
     *
     * @return дирректорию в которой ведется поиск файлов
     */
    public static String getControlDir(){
        return properties.getProperty("WorkDir");
    }

    /**
     *
     * @return файлы старше скольки дней удалять из дирректории.
     */
    public static String getDellDate(){
        return properties.getProperty("DellDate");
    }

    /**
     *
     * @return данный метод возвращает путь к папке в которую будут генерироваться отчеты
     */
    public static String getReportDir(){
        return properties.getProperty("ReportDir");
    }

    /**
     *
     * @return - путь к справочнику известных назначений вызовов
     */
    public static String getSprOutCallfile(){
        return properties.getProperty("SprOutCall");
    }

    /**
     *
     * @return - путь к справочнику внутренних номеров
     */
    public static String getSprFXSfile(){
        return properties.getProperty("SprFXS");
    }

    /**
     *
     * @return
     */
    public static String getArchiveNamePrefix(){
        return properties.getProperty("ArchiveNamePrefix");
    }

    public static String getOutputDir(){
        return properties.getProperty("OutputDir");
    }

    public static String getReceiverMail(){
        return properties.getProperty("ReceiverMail");
    }

    public static String getSenderMail(){
        return properties.getProperty("SenderMail");
    }
    public static String getSmtpServer(){
        return properties.getProperty("SmtpServer");
    }
    public static String getSmtpPort(){
        return properties.getProperty("SmtpPort");
    }

    public static String getSubjectLine(){
        return properties.getProperty("SubjectLine");
    }
}
