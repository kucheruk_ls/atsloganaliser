package ru.lkservis.atsloganalyser;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipArhiver {
    //создаем zip архив

    /**
     *
     * @param source
     * @return - возвращает Path созданного архива, для дальнейшей отправки его на мыло
     * @throws Exception
     */
    public Path createZip(Path source) throws Exception {
        // Проверяем, существует ли директория, где будет создаваться архив
        // При необходимости создаем ее
        Path zipDirectory = Paths.get(ConfigReader.getReportDir());
        if (Files.notExists(zipDirectory))
            Files.createDirectories(zipDirectory);

        // Создаем zip поток
        Path zipPath = zipNameCreator();
        try (ZipOutputStream zipOutputStream = new ZipOutputStream(Files.newOutputStream(zipPath))) {

            if (Files.isDirectory(source)) {
                // Если архивируем директорию, то нужно получить список файлов в ней
                FileManager fileManager = new FileManager(source);
                List<Path> fileNames = fileManager.getFileList();

                // Добавляем каждый файл в архив
                for (Path fileName : fileNames) {
                    addNewZipEntry(zipOutputStream, source, fileName);
                }
            } else if (Files.isRegularFile(source)) {

                // Если архивируем отдельный файл, то нужно получить его директорию и имя
                addNewZipEntry(zipOutputStream, source.getParent(), source.getFileName());
            } else {

                // Если переданный source не директория и не файл, бросаем исключение
                throw new PathIsNotFoundException();
            }
        }
        return zipPath;
    }

    /**
     *
     * @return метод возвращает имя зип архива
     */
    public Path zipNameCreator(){
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dates = simpleDateFormat.format(date);
        Path outputDir = Paths.get(ConfigReader.getOutputDir());
        if(Files.notExists(outputDir)){
            try{
                Files.createDirectories(outputDir);
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        Path path = Paths.get(ConfigReader.getOutputDir()+ConfigReader.getArchiveNamePrefix()+"_"+dates+".zip");
        return path;
    }

    /**
     * метод добавляет файлы в зип архив
     * @param zipOutputStream
     * @param filePath
     * @param fileName
     * @throws Exception
     */
    private void addNewZipEntry(ZipOutputStream zipOutputStream, Path filePath, Path fileName) throws Exception {
        Path fullPath = filePath.resolve(fileName);
        try (InputStream inputStream = Files.newInputStream(fullPath)) {
            ZipEntry entry = new ZipEntry(fileName.toString());

            zipOutputStream.putNextEntry(entry);

            copyData(inputStream, zipOutputStream);

            zipOutputStream.closeEntry();
        }
    }

    /**
     * метод копирует данные
     * @param in
     * @param out
     * @throws Exception
     */
    private void copyData(InputStream in, OutputStream out) throws Exception {
        byte[] buffer = new byte[8 * 1024];
        int len;
        while ((len = in.read(buffer)) > 0) {
            out.write(buffer, 0, len);
        }
    }
}
