package ru.lkservis.atsloganalyser;


import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class LogAnalyser {

    static String wd = ConfigReader.getControlDir();
    static File workDir = new File(wd);




    /**
     * 1. Проходимся по файлам в рабочей дирректории. Если файл имеет префикс "done-" и дату формирования(взятую из имени
     * файла) старше DellDate дней удаляем такие файлы.
     * 2. Будем разбирать файлы не отмеченные префиксом "done-"
     * @return - метод возвращает HashMap содержащую в качестве ключа внутренний номер абонента а в качестве значения
     * ArrayList<String> содержащий телефонные звонки абонента на номера начинающиеся на цифру 8
     */
    public static HashMap<String,ArrayList<String>> run(){
        HashMap<String, ArrayList<String>> MapCall = new HashMap<>();//все выбранные звонки.
        boolean test = workDir.isDirectory();


    if(workDir.isDirectory()){
       File[] listWorkDir = workDir.listFiles();
       //пройдемся по массиву файлов и, в случае если есть файлы старше даты указанной в параметре DellDate удалим их из
        //рабочей дирректории
        Date segodnya = new Date();
        for(File f:listWorkDir) {
            //Boolean testTmp = f.getName().startsWith("done-");
            if (f.isFile() && f.getName().startsWith("done-")) {
                long dd = ((segodnya.getTime() - fileDate(f).getTime()) / (1000 * 60 * 60 * 24));
                int fd = Integer.parseInt(ConfigReader.getDellDate());
                if (dd > fd) {
                    f.delete();
                }
            }
            if (f.isFile()){
                if (!f.getName().startsWith("done-")) {
                    FileReader fileReader;
                    try {
                        fileReader = new FileReader(f);
                        BufferedReader bufferedReader = new BufferedReader(fileReader);
                        String str = null;
                        while ((str = bufferedReader.readLine()) != null) {
                            if (InterSityCall(str)) { //исходящий звонок на 8, будем разбирать.
                                String s = str;
                                String[] sttCall = str.split(" ");
                                ArrayList<String> NumCall = new ArrayList<>(); // список звонков с одного внешнего номера.

                                for (String s1 : sttCall) {
                                    if(!s1.equals("")){
                                        NumCall.add(s1.trim());

                                    }
                                }

                                //поместим в массив звонков по номерам.
                                if(MapCall.containsKey(NumCall.get(2))){
                                    StringBuilder sb = new StringBuilder();
                                    sb.append(NumCall.get(0));
                                    sb.append(" ");
                                    sb.append(NumCall.get(1));
                                    sb.append(" ");
                                    sb.append(NumCall.get(4));
                                    sb.append(" ");
                                    sb.append(NumCall.get(5));
                                    sb.append(" ");
                                    if(SprOutCall.getSprOutCall().containsKey(NumCall.get(4))){
                                        sb.append(SprOutCall.getSprOutCall().get(NumCall.get(4)));
                                    }else {
                                        sb.append("_____________________________");
                                    }
                                    MapCall.get(NumCall.get(2)).add(sb.toString());

                                }else{
                                    ArrayList<String> list = new ArrayList<>();
                                    StringBuilder sb = new StringBuilder();
                                    sb.append(NumCall.get(0));
                                    sb.append(" ");
                                    sb.append(NumCall.get(1));
                                    sb.append(" ");
                                    sb.append(NumCall.get(4));
                                    sb.append(" ");
                                    sb.append(NumCall.get(5));
                                    sb.append(" ");
                                    if(SprOutCall.getSprOutCall().containsKey(NumCall.get(4))){
                                        sb.append(SprOutCall.getSprOutCall().get(NumCall.get(4)));
                                    }else {
                                        sb.append("_____________________________");
                                    }

                                    list.add(sb.toString());
                                    MapCall.put(NumCall.get(2),list);
                                }


                            }

                        }

                        bufferedReader.close();
                        fileReader.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }



                    File tmpName = new File(f.getParent()+"\\done-"+f.getName());
                    Boolean b = f.renameTo(tmpName);
                    if(!b){
                        System.out.println("Файл переименовать не удалось");
                    }
                }

        }
        }



       //тестовый вывод содержимого рабочей дирректории
       //for(File f:listWorkDir){
       //    System.out.println(f.getName().toString());
      // }


    }
        return MapCall;
    }

    /**
     * данный метод получает File с именем вида done-YYYY-DD-MM.log и возвращает дату формирования имени файла
     * @param file
     * @return возвращает дату формирования имени файла
     */
    private static Date fileDate(File file){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
        Date date = new Date();
        String testtmp = file.getName();
        String dateToFileName = (file.getName().substring(5,file.getName().indexOf(".log")));//.replace("-","\\")
        try{
            date = simpleDateFormat.parse(dateToFileName);
        }catch (ParseException e){
            e.printStackTrace();
        }
        return date;
    }

    /**
     * метод определяет начанается ли номер адресата звонка на 8, проверяется стоит ли "8" в 25 позиции строки
     * @param str - строка звонка из лога АТС
     * @return true если номер адресата звонка начинается с 8 - все просто.
     */
    private static boolean InterSityCall(String str){
        Boolean result = false;
        if(str!=null&&!str.equals("")) {


            String tmps = str.substring(0,1);
            if(!str.substring(0,1).equals(" ")) {
                String[] tmp = str.split(" ");
                if(tmp.length>6) {
                    String num = tmp[6];
                    int i = num.length();
                    boolean e = num.startsWith("8");
                    if (num.startsWith("8") && num.length() > 10) {
                        result = true;
                    }
                }

            }

        }
        return result;
    }




}
