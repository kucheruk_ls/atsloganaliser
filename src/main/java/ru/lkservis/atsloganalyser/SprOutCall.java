package ru.lkservis.atsloganalyser;

import java.io.*;
import java.util.HashMap;

/**
 * класс возвращает список известных адресатов звонков
 */
public class SprOutCall {
    static File spr = new File(ConfigReader.getSprOutCallfile());

    public static HashMap<String, String> getSprOutCall(){
        HashMap<String, String> sprMap = new HashMap<>();
        try{
            BufferedReader bufferedReader = new BufferedReader(new FileReader(spr.toString()));
            String str;
            while ((str=bufferedReader.readLine())!=null) {
                if(!str.startsWith("#")) {
                    String[] tmp = str.split(" ");
                    sprMap.put(tmp[0], tmp[1]);
                }
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        return sprMap;
    }
}
