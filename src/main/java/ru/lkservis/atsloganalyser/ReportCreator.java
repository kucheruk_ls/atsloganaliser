package ru.lkservis.atsloganalyser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * данный класс будет создавать текстовые отчеты о звонках в папке ReportDir
 */
public class ReportCreator {
    File ReportDir = new File(ConfigReader.getReportDir());

    LogAnalyser logAnalyser = new LogAnalyser();
    //HashMap<String, ArrayList<String>> callMap = logAnalyser.run();

    public void reportGenerator(HashMap<String, ArrayList<String>> callMap){

        if(!ReportDir.exists()){
            ReportDir.mkdir();
        }

        for(Map.Entry<String, ArrayList<String>> entry:callMap.entrySet()){
            BufferedWriter bufferedWriter = null;
            try{
                bufferedWriter = new BufferedWriter(new FileWriter(ReportDir.toString()+"\\"+entry.getKey()+".txt"));
                String fio = SprFXS.getSprFXS().get(entry.getKey());
                bufferedWriter.write(entry.getKey()+" "+fio);
                bufferedWriter.newLine();
                bufferedWriter.newLine();
                StringBuilder sb = new StringBuilder();//0145
                for(int i=0;i<entry.getValue().size();i++){
                    bufferedWriter.write(entry.getValue().get(i));
                    bufferedWriter.newLine();
                }
                bufferedWriter.newLine();
                bufferedWriter.newLine();
                bufferedWriter.newLine();
                bufferedWriter.write(" ________________________________"+fio);
                bufferedWriter.flush();
                bufferedWriter.close();
            }catch (IOException e){
                e.printStackTrace();
            }finally {
                try{
                    bufferedWriter.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }


            //---
            System.out.println(entry.getKey()+" "+SprFXS.getSprFXS().get(entry.getKey()));
            StringBuilder sb = new StringBuilder();//0145
            for(int i=0;i<entry.getValue().size();i++){
                System.out.println(entry.getValue().get(i));
            }
        }
    }



}
